$(function () {
		var tempData = [0, 1, 9, 6, 2, 12, 10, 15, 12, 6, 7, 15, 2, 0, 7, 3, 9, 2, 17, 5, 4, 6, 2, 12, 7, 8, 7, 21, 10, 25, 4, 4, 14, 6, 21, 10, 8, 7, 3, 9, 15, 13, 12, 14, 25, 23, 12]

		var years = [1940, 1941, 1943, 1959, 1960, 1961, 1962, 1963, 1973, 1974, 1975, 1976, 1977, 1978, 1979, 1980, 1981, 1982, 1983, 1984, 1985, 1986, 1987, 1988, 1989, 1990, 1991,
         1992, 1993, 1994, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2004, 2005, 2006, 2007, 2008, 2010, 2011, 2012, 2013, 2014];

    $('.trop_pha').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Počet tropických dní v Brně'
        },
        subtitle: {
        	align: 'right',
            verticalAlign: 'bottom',
            x: 0,
            y: -5,
            text: '<i style="font-size:9px"><a target="_blank" href="https://www.ncdc.noaa.gov/cdo-web/">archiv NOAA</a>; čísla nemusí být kompletní,' + 
            '<br>data českého <a target="_blank" href="http://portal.chmi.cz/">ČHMÚ</a> nejsou volně dostupná</i>',
            useHTML: true
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories: years,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Počet dní'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">Rok {point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">Tropické dny: </td>' +
                '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Tropické dny (maximální teplota 30 °C a více)',
            id: 'dataset',
            data: tempData
        }, {
        	name: 'Trend',
            linkedTo: 'dataset',
            showInLegend: false,
            enableMouseTracking: false,
            type: 'trendline',
            color: 'red',
            dashStyle: 'dot',
            algorithm: 'linear'
        }]
    });
});