---
title: "Mapa horkého Brna: Na nádražích a v průmyslových zónách je až o deset stupňů tepleji"
perex: "Průmyslové zóny, nádraží a nákupní centra se v letních dnech ohřejí na teploty i o deset stupňů vyšší, než jaké jsou v parcích a zatravněných oblastech. Ukazují to snímky ze satelitu Landsat 8, které umožňují odhadnout teplotu povrchu."
description: "Brno hledá způsob, jak se přizpůsobit nárůstu teploty. Testují se i zatravněné střechy"
authors: ["Karolína Peřestá", "Jan Cibulka"]
published: "22. srpna 2016"
socialimg: https://interaktivni.rozhlas.cz/horko-v-brne/media/socimg.png
# coverimg: https://interaktivni.rozhlas.cz/brexit/media/cover.jpg
# coverimg_note: "Foto <a href='#'>ČTK</a>"
url: "horko-v-brne"
libraries: [jquery, highcharts,  https://interaktivni.rozhlas.cz/tools/highcharts-technical-indicators/0.0.1.min.js, https://js.arcgis.com/3.17/]
styles: [https://js.arcgis.com/3.17/esri/css/esri.css]
recommended:
  - link: https://interaktivni.rozhlas.cz/horko-ve-mestech/
    title: Mapa pražského horka: Stromy ochladí okolí o několik stupňů
    perex: Hlavní město hledá cesty, jak se vypořádat s tropickými vedry.
    image: https://interaktivni.rozhlas.cz/horko-ve-mestech/media/socimg.png
---

Největší horko je na velkých tmavých plochách, jakými jsou brownfieldy, nádraží, velká parkoviště či střechy továrních hal. Chladněji je naopak u řeky nebo v blízkosti stromů. Velké porosty, jako lesoparky, si udržují až o osm stupňů nižší teploty než zástavba v jejich okolí; dva až tři stupně teploty ale zvládnou srazit i osamělé stromy, trávník mezi domy nebo vnitrobloky. To je také důvod, proč je například na Lesné – jednom z nejstarších a nejzelenějších brněnských sídlišť – příjemnější teplota než mezi panelovými domy jinde ve městě.

Na mapě si můžete prohlédnout dopolední květnové teploty na konkrétním místě v Brně. Údaje pocházejí z 9. května 2016, kdy se k vyšším denním teplotám přidalo jasné nebe, takže měření není zkresleno lokálními mraky.

<aside id="mapa" class="big">
	<div id="search"></div>
	<div style="height:600px;" id="map">
</aside>

*Satelit je schopen zachytit [elektromagnetické záření](https://cs.wikipedia.org/wiki/Elektromagnetick%C3%A9_spektrum) z různých typů povrchů. Následnými výpočty je možné přibližně určit jejich teplotu ve chvíli snímkování. Zjištěné teploty jsou přitom vyšší, než jaké známe z předpovědí počasí: meteorologické teploměry měří teplotu ve stínu, zatímco satelit zachytil přibližné hodnoty, které měly střechy a silnice vystavené slunci.*

Stejná data Český rozhlas zpracoval také pro Prahu: podívejte se, [proč jsou v létě Vinohrady příjemnější než Smíchov](https://interaktivni.rozhlas.cz/horko-ve-mestech/).

## Tropických dní rychle přibývá, pomáhají zatravněné střechy

V Brně je problém s rozpálenými plochami o to závažnější, že v posledních letech přibývá tropických dní. Počet dní s maximální denní teplotou nad 30 °C se od čtyřicátých let, kdy jsou dostupná měření, zvyšuje.

<aside class="big">
   <div style="height:400px;" class="trop_pha"></div>
</aside>

Na brněnském magistrátu si v návaznosti na rostoucí počet tropických dnů nechali zpracovat studii od odborníků z Akademie věd. Sálající místa tedy mají zmapována a připravují opatření, která by pobyt ve velkoměstě v horkých dnech měla vylepšit. „Při plánování rekonstrukce ulic s tím počítáme, umisťujeme tam stromy nebo necháváme volné plochy právě proto, aby se zabránilo přehřívání,“ vysvětluje mluvčí Filip Poňuchálek. V září chce vedení města představit konkrétní nápady.

Nejen na magistrátu hledají způsoby, jak rozpáleným místům ulevit. Ve vnitrobloku s názvem Otevřená zahrada na Údolní ulici uplynulé dva roky testovali zatravněnou střechu. „Máme dvě pasivní budovy, kde podrobně měříme spotřebu energie a vody,“ říká ředitel Nadace partnerství Miroslav Kundrata. „Díky tomu můžeme srovnávat, jak je na nich zelená střecha efektivní. Výsledky ukazují, že bychom skutečně měli zachovat co největší plochy přirozených travních porostů.“ Kromě samotné zeleně podle něj zabraňuje přehřívání prostor i zemina – když naprší, ochladí okolí o několik stupňů.
